//
//  SecondNumbers.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SecondNumbers : NSObject {

	NSMutableArray *_clocknumbers;
	int _curframe;
	NSMutableString *_curclockstring;
	int _dmin;
	int _dhour;
	UIImage *frame;
}

- (void) Update;
- (void) Initialise;
- (void) Paint;
- (void) setClockstring:(NSString *)clockstring;
- (void) PaintString;

@end
