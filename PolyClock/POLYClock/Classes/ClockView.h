//
//  ClockView.h
//  POLYClock
//
//  Created by Danny Draper on 29/04/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockNumbers.h"
#import "SecondNumbers.h"
#import "DaysTemplate.h"
#import "MonthsTemplate.h"
#import "SmallNumbers.h"
#import "AlarmNumbers.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>

typedef enum {
	SndClockButton,
	SndClockButtonHigh,
	SndAlarmSound1,
	SndAlarmSound2,
	SndAlarmSound3,
	SndAlarmSound4,
	SndAlarmSound5,
	SndAlarmSound6,
	SndAlarmSound7,
	SndAlarmSound8,
	SndAlarmSound9,
	SndAlarmSound10
} ClockSound;

typedef enum {
	AdjHourUp,
	AdjHourDown,
	AdjMinuteUp,
	AdjMinuteDown,
	AdjSecondUp,
	AdjSecondDown,
	AdjNone
} AlarmAdjustMode;

@interface ClockView : UIView<AVAudioPlayerDelegate> {
	
	IBOutlet UIImageView *imgview_announce;
	IBOutlet UIImageView *imgview_hourmode;
	IBOutlet UIImageView *imgview_alarmonoff;
	IBOutlet UIImageView *imgview_alarmsoundprev;
	IBOutlet UIImageView *imgview_alarmsoundnext;
	IBOutlet UIImageView *imgview_brightness;
	
	
	IBOutlet UIImageView *imgview_titlesectionlandscape;
	IBOutlet UIImageView *imgview_middlesectionlandscape;
	IBOutlet UIImageView *imgview_bottomsectionlandscape;
	
	IBOutlet UIImageView *imgview_titlesectionportrait;
	IBOutlet UIImageView *imgview_middlesectionportrait;
	IBOutlet UIImageView *imgview_bottomsectionportrait;
	IBOutlet UIImageView *imgview_lowerleftsectionportrait;
	
	IBOutlet UIImageView *imgview_alarmactive;
	IBOutlet UIImageView *imgview_ampmindicator;
	
	IBOutlet UIImageView *imgview_alert;
	
	IBOutlet UIButton *btn_alarmsnd;
	IBOutlet UIButton *btn_alarm;
	IBOutlet UIButton *btn_alarmtime;
	
	IBOutlet UIButton *btn_hourup;
	IBOutlet UIButton *btn_hourdown;
	IBOutlet UIButton *btn_minuteup;
	IBOutlet UIButton *btn_minutedown;
	IBOutlet UIButton *btn_secondup;
	IBOutlet UIButton *btn_seconddown;
	
	IBOutlet UIButton *btn_timermode;
	IBOutlet UIButton *btn_alarmmode;
	
	IBOutlet UIButton *btn_snooze;
	IBOutlet UIButton *btn_dismiss;
	
	IBOutlet UIButton *btn_starttimer;
	IBOutlet UIButton *btn_stoptimer;
	
	
	UIImage *alarmsoundlabel;
	CGPoint _alarmsoundlabel_location;
	
	ClockNumbers *_clocknumbers;
	SecondNumbers *_secondnumbers;
	
	SmallNumbers *_daynumbers;
	DaysTemplate *_daystemplate;
	SmallNumbers *_yearnumbers;
	MonthsTemplate *_monthstemplate;
	AlarmNumbers *_alarmnumbers;
	
	NSDateFormatter* _formatter;
	NSLocale *_uslocale;
	NSCalendar *_gregorian;
	
	NSInteger _numericmonth;
	NSInteger _numericweekday;
	int _currentminute;
	int _currenthour;
	bool _ispm;
	bool _12hourmode;
	bool _alarmactive;
	bool _userdismissedalert;
	
	int _currentalarmminute;
	int _currentalarmhour;
	int _alarmbuttonrepeat;
	int _currentalarmsound;
	int _currentbrightness;
	
	bool _alertactive;
	bool _timermode;
	
	NSTimer *clockTimer;
	
	NSMutableString *_strtime;
	
	bool _audioplaying;
	AVAudioPlayer* _myaudioplayer;
	
	NSTimer *announceTimer;
	int _iannouncenumber;
	bool _timeannouncementplaying;
	AlarmAdjustMode _alarmadjustmode;
	bool _alarmadjustedbytimer;
	NSTimer *_alarmadjusttimer;
	NSTimer *_alerttimer;
	NSTimer *snoozeTimer;
	bool _alertflashactive;
	
	bool _landscapemode;
	
	int _currenttimerhour;
	int _currenttimerminute;
	int _currenttimersecond;
	
	int _backuptimerhour;
	int _backuptimerminute;
	int _backuptimersecond;	
	bool _timeractive;
	NSTimer *_timertimer;	
}

@property (retain, nonatomic) UIImageView *imgview_announce;
@property (retain, nonatomic) UIImageView *imgview_hourmode;
@property (retain, nonatomic) UIImageView *imgview_alarmonoff;
@property (retain, nonatomic) UIImageView *imgview_alarmsoundprev;
@property (retain, nonatomic) UIImageView *imgview_alarmsoundnext;
@property (retain, nonatomic) UIImageView *imgview_brightness;

@property (retain, nonatomic) UIImageView *imgview_titlesectionlandscape;
@property (retain, nonatomic) UIImageView *imgview_middlesectionlandscape;
@property (retain, nonatomic) UIImageView *imgview_bottomsectionlandscape;

@property (retain, nonatomic) UIImageView *imgview_titlesectionportrait;
@property (retain, nonatomic) UIImageView *imgview_middlesectionportrait;
@property (retain, nonatomic) UIImageView *imgview_bottomsectionportrait;
@property (retain, nonatomic) UIImageView *imgview_lowerleftsectionportrait;
@property (retain, nonatomic) UIImageView *imgview_alarmactive;
@property (retain, nonatomic) UIImageView *imgview_ampmindicator;

@property (retain, nonatomic) UIImageView *imgview_alert;

@property (retain, nonatomic) UIButton *btn_alarmsnd;
@property (retain, nonatomic) UIButton *btn_alarm;
@property (retain, nonatomic) UIButton *btn_alarmtime;

@property (retain, nonatomic) UIButton *btn_hourup;
@property (retain, nonatomic) UIButton *btn_hourdown;
@property (retain, nonatomic) UIButton *btn_minuteup;
@property (retain, nonatomic) UIButton *btn_minutedown;
@property (retain, nonatomic) UIButton *btn_secondup;
@property (retain, nonatomic) UIButton *btn_seconddown;

@property (retain, nonatomic) UIButton *btn_timermode;
@property (retain, nonatomic) UIButton *btn_alarmmode;

@property (retain, nonatomic) UIButton *btn_snooze;
@property (retain, nonatomic) UIButton *btn_dismiss;

@property (retain, nonatomic) UIButton *btn_starttimer;
@property (retain, nonatomic) UIButton *btn_stoptimer;

- (IBAction) announceButtonUp:(id)sender;
- (IBAction) announcebuttonDown:(id)sender;

- (IBAction) hourmodeButtonUp:(id)sender;
- (IBAction) hourmodeButtonDown:(id)sender;

- (IBAction) alarmonoffButtonUp:(id)sender;
- (IBAction) alarmonoffButtonDown:(id)sender;

- (IBAction) alarmsoundprevButtonUp:(id)sender;
- (IBAction) alarmsoundprevButtonDown:(id)sender;

- (IBAction) alarmsoundnextButtonUp:(id)sender;
- (IBAction) alarmsoundnextButtonDown:(id)sender;

- (IBAction) brightnessButtonUp:(id)sender;
- (IBAction) brightnessButtonDown:(id)sender;

- (IBAction) alarmsndButton:(id)sender;
- (IBAction) alarmButton:(id)sender;
- (IBAction) alarmtimeButton:(id)sender;

- (IBAction) timermodeButton:(id)sender;
- (IBAction) alarmmodeButton:(id)sender;

- (IBAction) alarmMinuteup:(id)sender;
- (IBAction) alarmMinutedown:(id)sender;
- (IBAction) alarmHourup:(id)sender;
- (IBAction) alarmHourdown:(id)sender;

- (IBAction) snoozeButton:(id)sender;
- (IBAction) dismissButton:(id)sender;
- (IBAction) secondAlarmsndbutton:(id)sender;
- (IBAction) secondAlarmbutton:(id)sender;
- (IBAction)timerModebuttontriggered:(id)sender;
- (IBAction)alarmModebuttontriggered:(id)sender;
- (IBAction) alarmSecondup:(id)sender;
- (IBAction) alarmSeconduptriggered:(id)sender;
- (IBAction) alarmSeconddown:(id)sender;
- (IBAction) alarmSeconddowntriggered:(id)sender;
- (IBAction) startTimerbutton:(id)sender;
- (IBAction) stopTimerbutton:(id)sender;

- (void)setLandscapemode:(bool)landscape;
- (void)refreshClockTick;
- (void)GetNumericTime;
- (void)clockTick:(id)sender;
- (void)startClocktimer;
- (void)RefreshAlarmtime;
- (void)refreshAlarmactive_imageview;
- (void)loadAlarmsoundlabel:(int)soundnumber;
- (void) refreshAmpm_imageview;
- (void) configureAudioServices;
- (void)playSinglesound:(NSString *)resourcename;
- (void)playSinglesoundEx:(NSString *)resourcename;
- (void)playSound:(ClockSound)soundtoplay;
- (void)startAnnouncementTimer;
- (void)triggerAnnounce:(id)sender;
- (void)playNumericsound:(int)numeric;
- (void)play12Hournumeric:(int)numeric;
- (void)SecondAdjust:(bool)down;

- (IBAction) alarmMinuteuptriggered:(id)sender;
- (IBAction) alarmMinutedowntriggered:(id)sender;
- (IBAction) alarmHouruptriggered:(id)sender;
- (IBAction) alarmHourdowntriggered:(id)sender;
- (void)startAlarmadjust;
- (void) alarmAdjusttick:(id)sender;
- (void)alarmAdjust:(AlarmAdjustMode)adjustmode;
- (IBAction)alarmAdjustcancel:(id)sender;
- (void)HourAdjust:(bool)down;
- (void)MinuteAdjust:(bool)down;
- (void) refreshAlert;
- (void) startAlerttimer;
- (void)triggerAlert:(id)sender;
- (void)startSnooze;
- (void)snoozeTick:(id)sender;
- (void) stopAlert;
- (void)saveSettings;
- (void)loadSettings;
- (void)clearAllLocalnotifications;
- (void)scheduleGenericalarm;
- (void)scheduleAlarmForDate:(NSDate*)theDate;
- (void)RefreshTimertime;
- (void)stopTimer;
- (void)startTimer;
- (void)timerTick:(id)sender;

@end
