//
//  ClockView.m
//  POLYClock
//
//  Created by Danny Draper on 29/04/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "ClockView.h"


@implementation ClockView

@synthesize imgview_announce;
@synthesize imgview_hourmode;
@synthesize imgview_alarmonoff;
@synthesize imgview_alarmsoundprev;
@synthesize imgview_alarmsoundnext;
@synthesize imgview_brightness;

@synthesize imgview_titlesectionlandscape;
@synthesize imgview_middlesectionlandscape;
@synthesize imgview_bottomsectionlandscape;

@synthesize imgview_titlesectionportrait;
@synthesize imgview_middlesectionportrait;
@synthesize imgview_bottomsectionportrait;
@synthesize imgview_lowerleftsectionportrait;

@synthesize btn_alarmsnd;
@synthesize btn_alarm;
@synthesize btn_alarmtime;

@synthesize btn_hourup;
@synthesize btn_hourdown;
@synthesize btn_minuteup;
@synthesize btn_minutedown;

@synthesize btn_timermode;
@synthesize btn_alarmmode;

@synthesize imgview_alarmactive;
@synthesize imgview_ampmindicator;

@synthesize imgview_alert;
@synthesize btn_snooze;
@synthesize btn_dismiss;
@synthesize btn_secondup;
@synthesize btn_seconddown;

@synthesize btn_starttimer;
@synthesize btn_stoptimer;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		
		[_clocknumbers = [ClockNumbers new] retain];
		[_clocknumbers Initialise];
		
		[_secondnumbers = [SecondNumbers new] retain];
		[_secondnumbers Initialise];
		
		[_daynumbers = [SmallNumbers new] retain];
		[_daynumbers Initialise];
		[_daynumbers setLocation:200.0f :190.0f];
		
		[_yearnumbers = [SmallNumbers new] retain];
		[_yearnumbers Initialise];
		
		[_daystemplate = [DaysTemplate new] retain];
		[_daystemplate Initialise];
		[_daystemplate setLocation:200.0f :100.0f];
		
		[_monthstemplate = [MonthsTemplate new] retain];
		[_monthstemplate Initialise];
		[_monthstemplate setLocation:260.0f :190.0f];
		
		[_alarmnumbers = [AlarmNumbers new] retain];
		[_alarmnumbers Initialise];
		
		[_gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] retain];
		[_formatter = [NSDateFormatter new] retain];
		[_uslocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] retain];
		[_formatter setLocale:_uslocale];
		
		[_strtime = [NSMutableString new] retain];
		
		_currenttimerhour = 0;
		_currenttimerminute = 5;
		_currenttimersecond = 0;
		
		_currentalarmhour = 7;
		_currentalarmminute = 30;
		_alarmbuttonrepeat = 0;
		_currentalarmsound = 1;
		_12hourmode = true;
		_alertactive = false;
		_currentbrightness = 5;
		
		[self loadSettings];
		
		[self configureAudioServices];
		
		[self loadAlarmsoundlabel:_currentalarmsound];
		[self refreshAlert];
		[self RefreshAlarmtime];
		[self refreshAlarmactive_imageview];
		
		[self startClocktimer];
	
	}
	return self;
}


- (void)saveSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	
	[prefs setBool:_alarmactive forKey:@"ply-alarmactive"];
	[prefs setBool:_12hourmode forKey:@"ply-12hourmode"];
	[prefs setInteger:_currentalarmsound forKey:@"ply-currentalarmsound"];
	[prefs setInteger:_currentalarmhour forKey:@"ply-currentalarmhour"];
	[prefs setInteger:_currentalarmminute forKey:@"ply-currentalarmminute"];
	[prefs setInteger:_currenttimerhour forKey:@"ply-currenttimerhour"];
	[prefs setInteger:_currenttimerminute forKey:@"ply-currenttimerminute"];
	[prefs setInteger:_currenttimersecond forKey:@"ply-currenttimersecond"];
	[prefs setBool:true forKey:@"settingspresent"];
	[prefs synchronize];
}


- (void)loadSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	bool settingspresent = [prefs boolForKey:@"ply-settingspresent"];
	
	if (settingspresent == true) {
		
		_alarmactive = [prefs boolForKey:@"ply-alarmactive"];
		_12hourmode = [prefs boolForKey:@"ply-12hourmode"];
		_currentalarmsound = [prefs integerForKey:@"ply-currentalarmsound"];
		_currentalarmhour = [prefs integerForKey:@"ply-currentalarmhour"];
		_currentalarmminute = [prefs integerForKey:@"ply-currentalarmminute"];
		_currenttimerhour =  [prefs integerForKey:@"ply-currenttimerhour"];
		_currenttimerminute = [prefs integerForKey:@"ply-currenttimerminute"];
		_currenttimersecond = [prefs integerForKey:@"ply-currenttimersecond"];
	}
	
	if (_currentalarmsound == 0) {
		_currentalarmsound = 1;
	}
}

- (void) configureAudioServices {
	
	
	AudioSessionInitialize (NULL, NULL,	NULL,NULL);
	
	//UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
	UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
	AudioSessionSetProperty (kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
	
	
	UInt32 audioOverride =  true;
	AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof (audioOverride), &audioOverride);
	
	AudioSessionSetActive (true);
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	_audioplaying = false;
	if (_myaudioplayer != nil)
	{
		NSLog (@"Releasing Audioplayer...");
		[_myaudioplayer release];
	}
}

- (void)playSinglesoundEx:(NSString *)resourcename
{
	if (_audioplaying == true) {
		if (_myaudioplayer != nil)
		{
			[_myaudioplayer stop];
			[_myaudioplayer release];
		}
	}
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		
		_myaudioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];		
		[url release];
		
		[_myaudioplayer setVolume:1];
		[_myaudioplayer setDelegate:self];
		[_myaudioplayer prepareToPlay];
		[_myaudioplayer play];	
		_audioplaying = true;
	}
}

- (void)playSinglesound:(NSString *)resourcename
{
	[self playSinglesoundEx:resourcename];
}

- (void)playSound:(ClockSound)soundtoplay
{
	if (soundtoplay == SndClockButton)
	{
		[self playSinglesound:@"clockbutton"];
	}
	
	if (soundtoplay == SndClockButtonHigh)
	{
		[self playSinglesound:@"clockbuttonhigh"];
	}
	
	if (soundtoplay == SndAlarmSound1)
	{
		[self playSinglesound:@"clocksound1"];
	}
	
	if (soundtoplay == SndAlarmSound2)
	{
		[self playSinglesound:@"clocksound3"];
	}
	
	if (soundtoplay == SndAlarmSound3)
	{
		[self playSinglesound:@"clocksound4"];
	}
	
	if (soundtoplay == SndAlarmSound4)
	{
		[self playSinglesound:@"clocksound5"];
	}
	
	if (soundtoplay == SndAlarmSound5)
	{
		[self playSinglesound:@"clocksound6"];
	}
	
	if (soundtoplay == SndAlarmSound6)
	{
		[self playSinglesound:@"clocksound7"];
	}
	
	if (soundtoplay == SndAlarmSound7)
	{
		[self playSinglesound:@"clocksound8"];
	}
	
	if (soundtoplay == SndAlarmSound8)
	{
		[self playSinglesound:@"clocksound9"];
	}
	
	if (soundtoplay == SndAlarmSound9)
	{
		[self playSinglesound:@"clocksound10"];
	}
	
	if (soundtoplay == SndAlarmSound10)
	{
		[self playSinglesound:@"clocksound11"];
	}
}

- (void)playCurrentalarmsound
{
	if (_currentalarmsound == 1) {[self playSound:SndAlarmSound1];return;}
	if (_currentalarmsound == 2) {[self playSound:SndAlarmSound2];return;}
	if (_currentalarmsound == 3) {[self playSound:SndAlarmSound3];return;}
	if (_currentalarmsound == 4) {[self playSound:SndAlarmSound4];return;}
	if (_currentalarmsound == 5) {[self playSound:SndAlarmSound5];return;}
	if (_currentalarmsound == 6) {[self playSound:SndAlarmSound6];return;}
	if (_currentalarmsound == 7) {[self playSound:SndAlarmSound7];return;}
	if (_currentalarmsound == 8) {[self playSound:SndAlarmSound8];return;}
	if (_currentalarmsound == 9) {[self playSound:SndAlarmSound9];return;}
	if (_currentalarmsound == 10) {[self playSound:SndAlarmSound10];return;}
}

- (void)refreshAlarmactive_imageview
{
	if (_alarmactive == false) {
		[imgview_alarmactive setImage:[UIImage imageNamed:@"Off.png"]];
		[imgview_alarmactive sizeToFit];
	} else {
		[imgview_alarmactive setImage:[UIImage imageNamed:@"Active.png"]];
		[imgview_alarmactive sizeToFit];
	}
}

- (void) refreshAmpm_imageview
{
	if (_12hourmode == true) {
		if (_ispm == true) {
			[imgview_ampmindicator setImage:[UIImage imageNamed:@"PM.png"]];
		} else {
			[imgview_ampmindicator setImage:[UIImage imageNamed:@"AM.png"]];
		}
		
		[imgview_ampmindicator sizeToFit];
		imgview_ampmindicator.hidden = false;
	} else {
		imgview_ampmindicator.hidden = true;
	}
	
}

- (void) refreshAlert
{
	if (_alertactive == true) {
		imgview_alert.hidden = false;
		btn_snooze.hidden = false;
		btn_dismiss.hidden = false;
	} else {
		imgview_alert.hidden = true;
		btn_snooze.hidden = true;
		btn_dismiss.hidden = true;	
	}
}

- (IBAction) announceButtonUp:(id)sender
{
	imgview_announce.alpha = 1.0f;
	
	NSLog (@"Announce button pressed.");
	[self playSound:SndClockButton];
	
	
	if (_timeannouncementplaying == false) {
		_timeannouncementplaying = true;
		[self startAnnouncementTimer];
	}	
}

- (IBAction) announcebuttonDown:(id)sender
{
	imgview_announce.alpha = 0.5f;
}

- (IBAction) hourmodeButtonUp:(id)sender
{
	imgview_hourmode.alpha = 1.0f;
	
	if (_12hourmode == false) {
		_12hourmode = true;
	} else {
		_12hourmode = false;
	}
	
	[self refreshAmpm_imageview];
	[self setNeedsDisplay];
}

- (IBAction) hourmodeButtonDown:(id)sender
{
	imgview_hourmode.alpha = 0.5f;	
	[self playSound:SndClockButton];
}

- (IBAction) alarmonoffButtonUp:(id)sender
{
	imgview_alarmonoff.alpha = 1.0f;
	if (_alarmactive == false) {
		_alarmactive = true;
	} else {
		_alarmactive = false;
	}
	
	
	[self refreshAlarmactive_imageview];
	[self setNeedsDisplay];
}

- (IBAction) alarmonoffButtonDown:(id)sender
{
	imgview_alarmonoff.alpha = 0.5f;
	[self playSound:SndClockButton];
}

- (IBAction) alarmsoundprevButtonUp:(id)sender
{
	imgview_alarmsoundprev.alpha = 1.0f;

}

- (IBAction) alarmsoundprevButtonDown:(id)sender
{
	imgview_alarmsoundprev.alpha = 0.5f;	
	
	_currentalarmsound--;
	
	if (_currentalarmsound < 1) {
		_currentalarmsound = 10;
	}
	
	[self loadAlarmsoundlabel:_currentalarmsound];
	[self setNeedsDisplay];
	[self playCurrentalarmsound];
}

- (IBAction) alarmsoundnextButtonUp:(id)sender
{
	imgview_alarmsoundnext.alpha = 1.0f;
}

- (IBAction) alarmsoundnextButtonDown:(id)sender
{
	imgview_alarmsoundnext.alpha = 0.5f;	
	
	_currentalarmsound++;
	
	if (_currentalarmsound > 10) {
		_currentalarmsound = 1;
	}
	
	
	[self loadAlarmsoundlabel:_currentalarmsound];
	[self setNeedsDisplay];
	[self playCurrentalarmsound];
}

- (IBAction) brightnessButtonUp:(id)sender
{
	imgview_brightness.alpha = 1.0f;
	
	[self playSinglesound:@"clockbuttonhigh"];
	
	if (_currentbrightness < 5) {
		_currentbrightness++;
	} else {
		_currentbrightness = 1;
	}
	
	if (_currentbrightness == 1) {
		self.alpha = 0.2f;
	}
	
	if (_currentbrightness == 2) {
		self.alpha = 0.4f;
	}
	
	if (_currentbrightness == 3) {
		self.alpha = 0.6f;
	}
	
	if (_currentbrightness == 4) {
		self.alpha = 0.8f;
	}
	
	if (_currentbrightness == 5) {
		self.alpha = 1.0f;
	}
}

- (IBAction) brightnessButtonDown:(id)sender
{
	imgview_brightness.alpha = 0.5f;
}

- (IBAction) alarmsndButton:(id)sender
{
	
}

- (IBAction) alarmButton:(id)sender
{
	
}

- (IBAction) alarmtimeButton:(id)sender
{
	
}

- (IBAction) timermodeButton:(id)sender
{
	
}

- (IBAction) alarmmodeButton:(id)sender
{
	
}

- (IBAction) startTimerbutton:(id)sender
{
	[self playSound:SndClockButton];
	[self startTimer];
}

- (IBAction) stopTimerbutton:(id)sender
{
	[self playSound:SndClockButton];
	[self stopTimer];
}

- (IBAction) alarmSecondup:(id)sender
{
	[self alarmAdjust:AdjSecondUp];
}

- (IBAction) alarmSeconduptriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjSecondUp;
	[self startAlarmadjust];
}

- (IBAction) alarmSeconddown:(id)sender
{
	[self alarmAdjust:AdjSecondDown];
}

- (IBAction) alarmSeconddowntriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjSecondDown;
	[self startAlarmadjust];
}

- (IBAction) alarmMinuteup:(id)sender
{
	[self alarmAdjust:AdjMinuteUp];
}


- (IBAction) alarmMinuteuptriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjMinuteUp;
	[self startAlarmadjust];
}

- (IBAction) alarmMinutedown:(id)sender
{
	[self alarmAdjust:AdjMinuteDown];
}

- (IBAction) alarmMinutedowntriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjMinuteDown;
	[self startAlarmadjust];
}

- (IBAction) alarmHourup:(id)sender
{
	[self alarmAdjust:AdjHourUp];
}

- (IBAction) alarmHouruptriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjHourUp;
	[self startAlarmadjust];
}

- (IBAction) alarmHourdown:(id)sender
{
	[self alarmAdjust:AdjHourDown];
}

- (IBAction) alarmHourdowntriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjHourDown;
	[self startAlarmadjust];	
}

- (IBAction)alarmAdjustcancel:(id)sender
{
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		_alarmadjusttimer = nil;
	}
}

- (void)SecondAdjust:(bool)down
{
	if (_timermode == true) {
		if (down == true) {
			_currenttimersecond--;
			
			if (_currenttimersecond <= -1) {
				_currenttimersecond = 59;
			}
			
		} else {
			++_currenttimersecond;
			
			if (_currenttimersecond >= 60) {
				_currenttimersecond = 0;
			}
		}
		
		[self RefreshTimertime];
		[self setNeedsDisplay];
	}
}

- (void)MinuteAdjust:(bool)down
{
	if (_timermode == true) {
		
		if (down == true) {
			_currenttimerminute--;
			
			if (_currenttimerminute <= -1) {
				_currenttimerminute = 59;
			}
			
		} else {
			++_currenttimerminute;
			
			if (_currenttimerminute >= 60) {
				_currenttimerminute = 0;
			}
		}
		
		[self RefreshTimertime];
		[self setNeedsDisplay];
		
	} else {
		if (down == true) {
			_currentalarmminute--;
			
			if (_currentalarmminute <= -1) {
				_currentalarmminute = 59;
			}
			
		} else {
			++_currentalarmminute;
			
			if (_currentalarmminute >= 60) {
				_currentalarmminute = 0;
			}
		}
		
		[self RefreshAlarmtime];
		[self setNeedsDisplay];
	}
	
	
}


- (void)HourAdjust:(bool)down
{
	if (_timermode == true) {
		
		if (down == true) {
			_currenttimerhour--;
			
			if (_currenttimerhour <= -1) {
				_currenttimerhour = 99;
			}
			
		} else {
			++_currenttimerhour;
			
			if (_currenttimerhour >= 100) {
				_currenttimerhour = 0;
			}
		}
		
		[self RefreshTimertime];
		[self setNeedsDisplay];	
		
	} else {
		if (down == true) {
			_currentalarmhour--;
			
			if (_currentalarmhour <= -1) {
				_currentalarmhour = 23;
			}
			
		} else {
			++_currentalarmhour;
			
			if (_currentalarmhour >= 24) {
				_currentalarmhour = 0;
			}
		}
		
		[self RefreshAlarmtime];
		[self setNeedsDisplay];
	}
	
	
}

- (void)alarmAdjust:(AlarmAdjustMode)adjustmode
{
	if (_alarmadjustedbytimer == false) {
		if (adjustmode == AdjHourUp) {
			[self HourAdjust:false];
		}
		if (adjustmode == AdjHourDown) {
			[self HourAdjust:true];
		}
		if (adjustmode == AdjMinuteUp) {
			[self MinuteAdjust:false];
		}
		if (adjustmode == AdjMinuteDown) {
			[self MinuteAdjust:true];
		}
		if (adjustmode == AdjSecondUp) {
			[self SecondAdjust:false];
		}
		if (adjustmode == AdjSecondDown) {
			[self SecondAdjust:true];
		}
		
	}
	
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		_alarmadjusttimer = nil;
	}
}

- (void) alarmAdjusttick:(id)sender
{
	if (_alarmadjustmode == AdjHourUp)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:false];
	}
	
	if (_alarmadjustmode == AdjHourDown)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:true];
	}
	
	if (_alarmadjustmode == AdjMinuteUp)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:false];
	}
	
	if (_alarmadjustmode == AdjMinuteDown)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:true];
	}
	
	if (_alarmadjustmode == AdjSecondUp)
	{
		_alarmadjustedbytimer = true;
		[self SecondAdjust:false];
	}
	
	if (_alarmadjustmode == AdjSecondDown)
	{
		_alarmadjustedbytimer = true;
		[self SecondAdjust:true];
	}
}

- (void)startAlarmadjust
{
	//[self HourAdjust:false];
	_alarmadjustedbytimer = false;
	_alarmadjusttimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.3 target:self selector:@selector(alarmAdjusttick:) userInfo:nil repeats:TRUE];
	[_alarmadjusttimer retain];
}

- (void)loadAlarmsoundlabel:(int)soundnumber
{
	if (soundnumber == 1) {alarmsoundlabel = [UIImage imageNamed:@"Snd1.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 2) {alarmsoundlabel = [UIImage imageNamed:@"Snd2.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 3) {alarmsoundlabel = [UIImage imageNamed:@"Snd3.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 4) {alarmsoundlabel = [UIImage imageNamed:@"Snd4.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 5) {alarmsoundlabel = [UIImage imageNamed:@"Snd5.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 6) {alarmsoundlabel = [UIImage imageNamed:@"Snd6.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 7) {alarmsoundlabel = [UIImage imageNamed:@"Snd7.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 8) {alarmsoundlabel = [UIImage imageNamed:@"Snd8.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 9) {alarmsoundlabel = [UIImage imageNamed:@"Snd9.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 10) {alarmsoundlabel = [UIImage imageNamed:@"Snd10.png"]; [alarmsoundlabel retain];return;}
	
	
	
	alarmsoundlabel = nil;
}

- (void)RefreshAlarmtime
{
	NSNumber *mins;
	NSNumber *hours;
	
	[_strtime setString:@""];
	
	mins = [NSNumber numberWithInt:_currentalarmminute];
	hours = [NSNumber numberWithInt:_currentalarmhour];
	
	if ([hours intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[hours stringValue]];
	[_strtime appendString:@":"];
	
	if ([mins intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[mins stringValue]];
	
	[_alarmnumbers setClockstring:_strtime];
	
}

- (void)RefreshTimertime
{
	NSNumber *hours;
	NSNumber *mins;
	NSNumber *secs;
	
	[_strtime setString:@""];
	
	hours = [NSNumber numberWithInt:_currenttimerhour];
	mins = [NSNumber numberWithInt:_currenttimerminute];
	secs = [NSNumber numberWithInt:_currenttimersecond];
	
	if ([hours intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[hours stringValue]];
	[_strtime appendString:@":"];
	
	
	if ([mins intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[mins stringValue]];
	[_strtime appendString:@":"];
	
	if ([secs intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[secs stringValue]];
	
	[_alarmnumbers setClockstring:_strtime];
}

- (void)clockTick:(id)sender
{
	[self refreshClockTick];
	[self setNeedsDisplay];
}

- (void)startClocktimer
{
	clockTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.3 target:self selector:@selector(clockTick:) userInfo:nil repeats:TRUE];
	[clockTimer retain];
}

- (void)refreshClockTick
{	
	[self GetNumericTime];
	
	NSDate* date = [NSDate date];
	
	
	
	if (_12hourmode == true) {
		[_formatter setDateFormat:@"HH:mm"];
		
		if (_currenthour == 13) {[_formatter setDateFormat:@"1:mm"];}
		if (_currenthour == 14) {[_formatter setDateFormat:@"2:mm"];}		
		if (_currenthour == 15) {[_formatter setDateFormat:@"3:mm"];}
		if (_currenthour == 16) {[_formatter setDateFormat:@"4:mm"];}
		if (_currenthour == 17) {[_formatter setDateFormat:@"5:mm"];}
		if (_currenthour == 18) {[_formatter setDateFormat:@"6:mm"];}
		if (_currenthour == 19) {[_formatter setDateFormat:@"7:mm"];}
		if (_currenthour == 20) {[_formatter setDateFormat:@"8:mm"];}
		if (_currenthour == 21) {[_formatter setDateFormat:@"9:mm"];}
		if (_currenthour == 22) {[_formatter setDateFormat:@"10:mm"];}
		if (_currenthour == 23) {[_formatter setDateFormat:@"11:mm"];}
		if (_currenthour == 0) {[_formatter setDateFormat:@"12:mm"];}
		
		[self refreshAmpm_imageview];
	} else {
		[_formatter setDateFormat:@"HH:mm"];
	}
	
	NSString* str = [_formatter stringFromDate:date];
	[_clocknumbers setClockstring:str];
	
	
	[_formatter setDateFormat:@"ss"];
	str = [_formatter stringFromDate:date];
	[_secondnumbers setClockstring:str];
	
	[_formatter setDateFormat:@"d"];
	str = [_formatter stringFromDate:date];
	[_daynumbers setClockstring:str];
	
	[_formatter setDateFormat:@"yyyy"];
	str = [_formatter stringFromDate:date];
	[_yearnumbers setClockstring:str];
	
	[_formatter setDateFormat:@"yyyyMM.d"];
	str = [_formatter stringFromDate:date];
	//[_stardatenumbers setClockstring:str];
	
	[_formatter setDateFormat:@"EEEE"];
	str = [_formatter stringFromDate:date];
	[_daystemplate setNumericWeekday:_numericweekday];
	
	[_formatter setDateFormat:@"MMMM"];
	str = [_formatter stringFromDate:date];
	[_monthstemplate setNumericMonth:_numericmonth];
	
	
	if (_alarmactive == true) {
		
		if (_userdismissedalert == false) {
			if (_currentalarmhour == _currenthour)
			{
				if (_currentalarmminute == _currentminute) {
					
					if (_alertactive == false) {
						//_alertactive = true;
						[self startAlerttimer];
					}
					
				}
			}
		}
		
	}
	
	if (_currentalarmminute != _currentminute) {
		_userdismissedalert = false;
	}	
}


- (void)GetNumericTime
{
	NSDate* date = [NSDate date];
	NSDateComponents *weekdayComponents =[_gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
	
	//[_formatter setDateFormat:@"HH mm p"];
	
	
	//NSString* strtesttime = [_formatter stringFromDate:date];
	//NSLog (@"Test time: %@", strtesttime);
	
	[_formatter setDateFormat:@"HH"];
	NSString* strhour = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"mm"];
	NSString* strminute = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"MM"];
	NSString* strmonth = [_formatter stringFromDate:date];
	
	
	
	_currentminute = [strminute intValue];
	_currenthour = [strhour intValue];
	_numericweekday = [weekdayComponents weekday];
	_numericmonth = [strmonth intValue];
	
	
	
	if (_currenthour == 0) {_ispm = false;}
	if (_currenthour == 1) {_ispm = false;}
	if (_currenthour == 2) {_ispm = false;}
	if (_currenthour == 3) {_ispm = false;}
	if (_currenthour == 4) {_ispm = false;}
	if (_currenthour == 5) {_ispm = false;}
	if (_currenthour == 6) {_ispm = false;}
	if (_currenthour == 7) {_ispm = false;}
	if (_currenthour == 8) {_ispm = false;}
	if (_currenthour == 9) {_ispm = false;}
	if (_currenthour == 10) {_ispm = false;}
	if (_currenthour == 11) {_ispm = false;}
	if (_currenthour == 12) {_ispm = true;}
	if (_currenthour == 13) {_ispm = true;}
	if (_currenthour == 14) {_ispm = true;}
	if (_currenthour == 15) {_ispm = true;}
	if (_currenthour == 16) {_ispm = true;}
	if (_currenthour == 17) {_ispm = true;}
	if (_currenthour == 18) {_ispm = true;}
	if (_currenthour == 19) {_ispm = true;}
	if (_currenthour == 20) {_ispm = true;}
	if (_currenthour == 21) {_ispm = true;}
	if (_currenthour == 22) {_ispm = true;}
	if (_currenthour == 23) {_ispm = true;}
}


- (void)setLandscapemode:(bool)landscape;
{
	[self refreshAlarmactive_imageview];
	
	if (landscape == true) {
		_landscapemode = true;
		
		NSLog (@"Landscape mode has been triggered.");
		imgview_bottomsectionportrait.hidden = true;
		imgview_bottomsectionlandscape.hidden = false;
		
		imgview_middlesectionportrait.hidden = true;
		imgview_middlesectionlandscape.hidden = false;
		
		imgview_titlesectionportrait.hidden = true;
		imgview_titlesectionlandscape.hidden = false;
		
		imgview_lowerleftsectionportrait.hidden = true;
		
		//Lanscape button positions
		
		btn_alarm.frame = CGRectMake(706.0f, 444.0f, 109.0f, 78.0f);
		btn_alarmsnd.frame = CGRectMake(706.0f, 340.0f, 109.0f, 78.0f);
		
		btn_timermode.frame = CGRectMake(823.0f, 678.0f, 84.0f, 30.0f);
		btn_alarmmode.frame = CGRectMake(928.0f, 678.0f, 84.0f, 30.0f);
		
		imgview_alarmactive.frame = CGRectMake (840.0f, 448.0f, imgview_alarmactive.frame.size.width, imgview_alarmactive.frame.size.height);
		
		[_yearnumbers setLocation: 610.0f : 190.0f];
		
		_alarmsoundlabel_location = CGPointMake(843.0f, 345.0f);
		
		if (_timermode == true) {
			[_alarmnumbers setLocation:CGPointMake(738.0f, 551.0f)];
			btn_alarmtime.frame = CGRectMake(606.0f, 550.0f, 109.0f, 78.0f);
			
			btn_minuteup.frame = CGRectMake(840.0f, 636.0f, 29.0f, 26.0f);
			btn_minutedown.frame = CGRectMake(875.0f, 636.0f, 29.0f, 26.0f);
			
			btn_hourup.frame = CGRectMake(743.0f, 636.0f, 29.0f, 26.0f);
			btn_hourdown.frame = CGRectMake(778.0f, 636.0f, 29.0f, 26.0f);
			
			btn_secondup.hidden = false;
			btn_seconddown.hidden = false;
			
			btn_secondup.frame = CGRectMake(947.0f, 636.0f, 29.0f, 26.0f);
			btn_seconddown.frame = CGRectMake(982.0f, 636.0f, 29.0f, 26.0f);
			
			btn_starttimer.frame = CGRectMake (625.0f, 643.0f, 84.0f, 30.0f);
			btn_stoptimer.frame = CGRectMake (626.0f, 681.0f, 84.0f, 30.0f);
			
			btn_starttimer.hidden = false;
			btn_stoptimer.hidden = false;
			
		} else {
			[_alarmnumbers setLocation:CGPointMake(838.0f, 551.0f)];
			btn_alarmtime.frame = CGRectMake(706.0f, 550.0f, 109.0f, 78.0f);
			
			btn_minuteup.frame = CGRectMake(940.0f, 636.0f, 29.0f, 26.0f);
			btn_minutedown.frame = CGRectMake(975.0f, 636.0f, 29.0f, 26.0f);
			
			btn_hourup.frame = CGRectMake(843.0f, 636.0f, 29.0f, 26.0f);
			btn_hourdown.frame = CGRectMake(878.0f, 636.0f, 29.0f, 26.0f);
			
			btn_secondup.hidden = true;
			btn_seconddown.hidden = true;
			
			btn_starttimer.hidden = true;
			btn_stoptimer.hidden = true;
		}
		
	} else {
		_landscapemode = false;
		
		NSLog (@"Portrait mode has been triggered.");
		
		imgview_bottomsectionportrait.hidden = false;
		imgview_bottomsectionlandscape.hidden = true;

		imgview_middlesectionportrait.hidden = false;
		imgview_middlesectionlandscape.hidden = true;		
		
		imgview_titlesectionportrait.hidden = false;
		imgview_titlesectionlandscape.hidden = true;
		
		imgview_lowerleftsectionportrait.hidden = false;
		
		
		
		btn_alarm.frame = CGRectMake(412.0f, 663.0f, 109.0f, 78.0f);
		btn_alarmsnd.frame = CGRectMake(144.0f, 663.0f, 109.0f, 78.0f);
		
		btn_timermode.frame = CGRectMake(529.0f, 896.0f, 84.0f, 30.0f);
		btn_alarmmode.frame = CGRectMake(634.0f, 896.0f, 84.0f, 30.0f);
		
		imgview_alarmactive.frame = CGRectMake (540.0f, 670.0f, imgview_alarmactive.frame.size.width, imgview_alarmactive.frame.size.height);
		
		[_yearnumbers setLocation: 560.0f : 190.0f];
		
		_alarmsoundlabel_location = CGPointMake(273.0f, 670.0f);
		
		if (_timermode == true) {
			[_alarmnumbers setLocation:CGPointMake(442.0f, 768.0f)];
			btn_alarmtime.frame = CGRectMake(312.0f, 768.0f, 109.0f, 78.0f);
		
			btn_minuteup.frame = CGRectMake(546.0f, 854.0f, 29.0f, 26.0f);
			btn_minutedown.frame = CGRectMake(581.0f, 854.0f, 29.0f, 26.0f);
			
			btn_hourup.frame = CGRectMake(449.0f, 854.0f, 29.0f, 26.0f);
			btn_hourdown.frame = CGRectMake(484.0f, 854.0f, 29.0f, 26.0f);	
			
			btn_secondup.hidden = false;
			btn_seconddown.hidden = false;
			
			btn_secondup.frame = CGRectMake(647.0f, 854.0f, 29.0f, 26.0f);
			btn_seconddown.frame = CGRectMake(682.0f, 854.0f, 29.0f, 26.0f);
			
			btn_starttimer.frame = CGRectMake (213.0f, 773.0f, 84.0f, 30.0f);
			btn_stoptimer.frame = CGRectMake (214.0f, 811.0f, 84.0f, 30.0f);
			
			btn_starttimer.hidden = false;
			btn_stoptimer.hidden = false;
			
		} else {
			[_alarmnumbers setLocation:CGPointMake(542.0f, 768.0f)];
			btn_alarmtime.frame = CGRectMake(412.0f, 768.0f, 109.0f, 78.0f);
			
			btn_minuteup.frame = CGRectMake(646.0f, 854.0f, 29.0f, 26.0f);
			btn_minutedown.frame = CGRectMake(681.0f, 854.0f, 29.0f, 26.0f);
			
			btn_hourup.frame = CGRectMake(549.0f, 854.0f, 29.0f, 26.0f);
			btn_hourdown.frame = CGRectMake(584.0f, 854.0f, 29.0f, 26.0f);	
			
			btn_secondup.hidden = true;
			btn_seconddown.hidden = true;
			
			btn_starttimer.hidden = true;
			btn_stoptimer.hidden = true;
		}
		
		
	}
	

}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	
	[_clocknumbers PaintString];
	[_secondnumbers PaintString];
	[_alarmnumbers PaintString];
	
	
	if (_alertactive == false) {
		[_daynumbers PaintString];
		[_yearnumbers PaintString];
		[_daystemplate PaintString];
		[_monthstemplate PaintString];
	}
	
	
	if (alarmsoundlabel != nil) {
		if (alarmsoundlabel != nil) {
			[alarmsoundlabel drawAtPoint:_alarmsoundlabel_location];
		}
	}
	
}

- (void)play12Hournumeric:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"12"];}
	if (numeric == 1) {[self playSinglesound:@"1"];}
	if (numeric == 2) {[self playSinglesound:@"2"];}
	if (numeric == 3) {[self playSinglesound:@"3"];}
	if (numeric == 4) {[self playSinglesound:@"4"];}
	if (numeric == 5) {[self playSinglesound:@"5"];}
	if (numeric == 6) {[self playSinglesound:@"6"];}
	if (numeric == 7) {[self playSinglesound:@"7"];}
	if (numeric == 8) {[self playSinglesound:@"8"];}
	if (numeric == 9) {[self playSinglesound:@"9"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"1"];}
	if (numeric == 14) {[self playSinglesound:@"2"];}
	if (numeric == 15) {[self playSinglesound:@"3"];}
	if (numeric == 16) {[self playSinglesound:@"4"];}
	if (numeric == 17) {[self playSinglesound:@"5"];}
	if (numeric == 18) {[self playSinglesound:@"6"];}
	if (numeric == 19) {[self playSinglesound:@"7"];}
	if (numeric == 20) {[self playSinglesound:@"8"];}
	
	if (numeric == 21) {[self playSinglesound:@"9"];}
	if (numeric == 22) {[self playSinglesound:@"10"];}
	if (numeric == 23) {[self playSinglesound:@"11"];}
}

- (void)playNumericsound:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"0"];}
	if (numeric == 1) {[self playSinglesound:@"01"];}
	if (numeric == 2) {[self playSinglesound:@"02"];}
	if (numeric == 3) {[self playSinglesound:@"03"];}
	if (numeric == 4) {[self playSinglesound:@"04"];}
	if (numeric == 5) {[self playSinglesound:@"05"];}
	if (numeric == 6) {[self playSinglesound:@"06"];}
	if (numeric == 7) {[self playSinglesound:@"07"];}
	if (numeric == 8) {[self playSinglesound:@"08"];}
	if (numeric == 9) {[self playSinglesound:@"09"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"13"];}
	if (numeric == 14) {[self playSinglesound:@"14"];}
	if (numeric == 15) {[self playSinglesound:@"15"];}
	if (numeric == 16) {[self playSinglesound:@"16"];}
	if (numeric == 17) {[self playSinglesound:@"17"];}
	if (numeric == 18) {[self playSinglesound:@"18"];}
	if (numeric == 19) {[self playSinglesound:@"19"];}
	if (numeric == 20) {[self playSinglesound:@"20"];}
	
	if (numeric == 21) {[self playSinglesound:@"21"];}
	if (numeric == 22) {[self playSinglesound:@"22"];}
	if (numeric == 23) {[self playSinglesound:@"23"];}
	if (numeric == 24) {[self playSinglesound:@"24"];}
	if (numeric == 25) {[self playSinglesound:@"25"];}
	if (numeric == 26) {[self playSinglesound:@"26"];}
	if (numeric == 27) {[self playSinglesound:@"27"];}
	if (numeric == 28) {[self playSinglesound:@"28"];}
	if (numeric == 29) {[self playSinglesound:@"29"];}
	if (numeric == 30) {[self playSinglesound:@"30"];}
	
	if (numeric == 31) {[self playSinglesound:@"31"];}
	if (numeric == 32) {[self playSinglesound:@"32"];}
	if (numeric == 33) {[self playSinglesound:@"33"];}
	if (numeric == 34) {[self playSinglesound:@"34"];}
	if (numeric == 35) {[self playSinglesound:@"35"];}
	if (numeric == 36) {[self playSinglesound:@"36"];}
	if (numeric == 37) {[self playSinglesound:@"37"];}
	if (numeric == 38) {[self playSinglesound:@"38"];}
	if (numeric == 39) {[self playSinglesound:@"39"];}
	if (numeric == 40) {[self playSinglesound:@"40"];}
	
	if (numeric == 41) {[self playSinglesound:@"41"];}
	if (numeric == 42) {[self playSinglesound:@"42"];}
	if (numeric == 43) {[self playSinglesound:@"43"];}
	if (numeric == 44) {[self playSinglesound:@"44"];}
	if (numeric == 45) {[self playSinglesound:@"45"];}
	if (numeric == 46) {[self playSinglesound:@"46"];}
	if (numeric == 47) {[self playSinglesound:@"47"];}
	if (numeric == 48) {[self playSinglesound:@"48"];}
	if (numeric == 49) {[self playSinglesound:@"49"];}
	if (numeric == 50) {[self playSinglesound:@"50"];}
	
	if (numeric == 51) {[self playSinglesound:@"51"];}
	if (numeric == 52) {[self playSinglesound:@"52"];}
	if (numeric == 53) {[self playSinglesound:@"53"];}
	if (numeric == 54) {[self playSinglesound:@"54"];}
	if (numeric == 55) {[self playSinglesound:@"55"];}
	if (numeric == 56) {[self playSinglesound:@"56"];}
	if (numeric == 57) {[self playSinglesound:@"57"];}
	if (numeric == 58) {[self playSinglesound:@"58"];}
	if (numeric == 59) {[self playSinglesound:@"59"];}
	
}

- (void)triggerAnnounce:(id)sender
{
	
	if (_iannouncenumber == 0) {
		[self playSinglesound:@"Time"];
	}
	
	if (_iannouncenumber == 1) {
		if (_12hourmode == true) {
			[self play12Hournumeric:_currenthour];
		} else {
			[self playNumericsound:_currenthour];
		}
		
	}
	
	if (_iannouncenumber == 2) {
		if (_currentminute == 0) {
			if (_12hourmode == true) {
				[self playSinglesound:@"oclock"];
			} else {
				[self playSinglesound:@"hundred"];
			}
		} else {
			[self playNumericsound:_currentminute];
		}
		
	}
	
	if (_12hourmode == true) {
		if (_iannouncenumber == 3) {
			if (_ispm == true) {
				[self playSinglesound:@"pm"];
			} else {
				[self playSinglesound:@"am"];
			}
		}
	}
	
	if (_iannouncenumber >= 4) {
		[announceTimer invalidate];
		_timeannouncementplaying = false;
	}
	
	_iannouncenumber++;
}

- (void)startAnnouncementTimer
{
	_iannouncenumber = 0;
	announceTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1.2 target:self selector:@selector(triggerAnnounce:) userInfo:nil repeats:TRUE];
	[announceTimer retain];
	//clockTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(clockTick:) userInfo:nil repeats:TRUE];
}


- (void)triggerAlert:(id)sender
{
	if (_alertflashactive == true) {
		_alertflashactive = false;
		
		imgview_alert.hidden = false;
		[self playCurrentalarmsound];
		
	} else {
		
		_alertflashactive = true;
	
		imgview_alert.hidden = true;
	}
}

- (void) startAlerttimer
{
	_alertactive = true;
	
	[self setNeedsDisplay];
	[self refreshAlert];
	
	_alerttimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1.3 target:self selector:@selector(triggerAlert:) userInfo:nil repeats:TRUE];
	[_alerttimer retain];
}

- (IBAction) snoozeButton:(id)sender
{
	[self playSound:SndClockButton];
	
	_alertactive = false;
	_userdismissedalert = true;
	[self startSnooze];
	[self setNeedsDisplay];
}

- (IBAction) dismissButton:(id)sender
{
	[self playSound:SndClockButton];
	[self stopAlert];
}



- (void)snoozeTick:(id)sender
{
	[self startAlerttimer];
}

- (void)startSnooze
{
	
	[self stopAlert];
	// Set the snooze timer for 10 minutes
	snoozeTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)600 target:self selector:@selector(snoozeTick:) userInfo:nil repeats:FALSE];
	[snoozeTimer retain];
}


- (void) stopAlert
{
	_userdismissedalert = true;
	if (_alerttimer != nil) {
		[_alerttimer invalidate];
	}
	
	_alertactive = false;
	
	[self refreshAlert];
	[self setNeedsDisplay];
}

- (IBAction) secondAlarmsndbutton:(id)sender
{
	[self playCurrentalarmsound];
}

- (IBAction) secondAlarmbutton:(id)sender
{
	[self playSound:SndClockButton];
	
	if (_alarmactive == false) {
		_alarmactive = true;
	} else {
		_alarmactive = false;
	}
	
	
	[self refreshAlarmactive_imageview];
	[self setNeedsDisplay];
}


/////////////////////////////////////////////////////////////////////////
// LOCAL NOTIFICATION STUFF
/////////////////////////////////////////////////////////////////////////

- (void)scheduleGenericalarm
{
	if (_alarmactive == false) {
		return;
	}
	
	NSDate *now = [NSDate date];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
	
	
	NSDateComponents *alarm_components = [gregorian components:unitFlags fromDate:now];
	
	//int alarmday = [alarm_components day];
	//int alarmmonth = [alarm_components month];
	//int alarmyear = [alarm_components year];
	//int alarmhour = _currentalarmhour;
	//int alarmminute = _currentalarmminute;
	
	[alarm_components setHour:_currentalarmhour];
	[alarm_components setMinute:_currentalarmminute];
	
	//NSLog (@"Alarm Day: %i", alarmday);
	//NSLog (@"Alarm Month: %i", alarmmonth);
	//NSLog (@"Alarm Year: %i", alarmyear);
	//NSLog (@"Alarm Hour: %i", alarmhour);
	//NSLog (@"Alarm Minute: %i", alarmminute);
	
	
	NSDate *alarm_today = [gregorian dateFromComponents:alarm_components];
	
	//NSLog (@"Alarm Date is: %@", alarm_today);
	
	NSDateComponents *addcomp = [[NSDateComponents alloc] init];
	addcomp.day = 1;
	
	NSDate *alarm_tomorrow = [gregorian dateByAddingComponents:addcomp toDate:alarm_today options:0];
	
	//NSLog (@"Alarm Tomorrow: %@", alarm_tomorrow);
	
	if ([alarm_today timeIntervalSinceDate:now] >= 0) {
		//NSLog (@"Alarm today is in the future");
		
		[self scheduleAlarmForDate:alarm_today];
	} else {
		//NSLog (@"Alarm today is in the past");
		
		[self scheduleAlarmForDate:alarm_tomorrow];
	}
	
	//NSDate *now = [NSDate date];
	//NSDate *future = [now dateByAddingTimeInterval:10];
	
	
	
	[gregorian release];
	[addcomp release];
	//[self scheduleAlarmForDate:future];
}

- (void)clearAllLocalnotifications
{
	
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
		NSLog (@"Local notifications cleared.");
	}
}

- (void)scheduleAlarmForDate:(NSDate*)theDate
{
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
	}
	
	// Create a new notification
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	
	if (alarm)
	{
		alarm.fireDate = theDate;
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		alarm.repeatInterval = NSDayCalendarUnit;
		
		//if (_usingsound1 == true) {
		//alarm.soundName = @"clocksound1repeat.wav";
		//} else {
		alarm.soundName = @"clocksound3-repeat.WAV";
		//}
		
		alarm.alertBody = @"Alarm";
		
		[app scheduleLocalNotification:alarm];
	}
}



- (IBAction)alarmModebuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
	
	_timermode = false;
	[self RefreshAlarmtime];
	
	[self setLandscapemode:_landscapemode];
	
	//if (_landscapemode == true) {
		//[_scrollynumbers setLastcolumnvisible:!_timermode];
	//}
	
	[self setNeedsDisplay];
	
}



- (IBAction)timerModebuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
	
	_timermode = true;
	[self RefreshTimertime];
	
	[self setLandscapemode:_landscapemode];
	
	//if (_landscapemode == true) {
		//[_scrollynumbers setLastcolumnvisible:!_timermode];
	//}
	
	[self setNeedsDisplay];
	
}

- (void)timerTick:(id)sender {
	
	
	_currenttimersecond--;
	
	if (_currenttimersecond < 0) {
		_currenttimersecond = 59;
		
		_currenttimerminute--;
		
		if (_currenttimerminute < 0) {
			_currenttimerminute = 59;
			
			_currenttimerhour--;
			
			if (_currenttimerhour < 0) {
				
				// Timer has reached zero - sound the alert and reset!
				[self stopTimer];
				
				_currenttimerhour = _backuptimerhour;
				_currenttimerminute = _backuptimerminute;
				_currenttimersecond = _backuptimersecond;
				
				_timermode = false;
				
				[self RefreshAlarmtime];
				[self setLandscapemode:_landscapemode];
				[self setNeedsDisplay];
				
				//if (_landscapemode == true) {
				//	[_scrollynumbers setLastcolumnvisible:!_timermode];
				//}
				
				[self startAlerttimer];
				
				
			}
		}
	}
	
	if (_timermode == true) {
		[self RefreshTimertime];
		[self setNeedsDisplay];
	}
}

- (void)startTimer {
	
	if (_timeractive == false) {
		
		_backuptimerhour = _currenttimerhour;
		_backuptimerminute = _currenttimerminute;
		_backuptimersecond = _currenttimersecond;
		
		_timertimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(timerTick:) userInfo:nil repeats:TRUE];
		[_timertimer retain];
		_timeractive = true;
	}
}

- (void)stopTimer {
	
	if (_timeractive == true) {
		[_timertimer invalidate];
		_timertimer = nil;
		_timeractive = false;
	}
}

- (void)dealloc {
    [super dealloc];
}


@end
