//
//  POLYClockViewController.h
//  POLYClock
//
//  Created by Danny Draper on 29/04/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockView.h"

@interface POLYClockViewController : UIViewController {

	IBOutlet ClockView *clockview;
	
}

@property (retain, nonatomic) ClockView *clockview;

- (void) saveSettings;

@end

