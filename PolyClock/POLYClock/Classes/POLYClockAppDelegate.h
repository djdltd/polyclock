//
//  POLYClockAppDelegate.h
//  POLYClock
//
//  Created by Danny Draper on 29/04/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class POLYClockViewController;

@interface POLYClockAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    POLYClockViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet POLYClockViewController *viewController;

@end

