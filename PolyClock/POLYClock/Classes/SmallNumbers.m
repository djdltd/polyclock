//
//  SmallNumbers.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "SmallNumbers.h"


@implementation SmallNumbers


- (void) Initialise
{
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	
	CGFloat startx = 20.0f;
	CGFloat starty = 13.0f;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	//CGPoint currentFramelocation;
	
	_clocknumbers = [NSMutableArray new];
	_curclockstring = [NSMutableString new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:@"SmallNumbers.png"];
	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	_xloc = 10.0f;
	_yloc = 10.0f;
	
	// First level of numbers
	for (i=0;i<11;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(25.0f, 57.0f);	
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect)];
		
		[_clocknumbers addObject:frame1];
		
		xoffset+=25.0f;
	}
}

- (void) setLocation:(CGFloat) xloc:(CGFloat) yloc
{
	_xloc = xloc;
	_yloc = yloc;
}

- (void) setClockstring:(NSString *)clockstring
{
	[_curclockstring setString:clockstring];
	
}

- (void) PaintString
{
	int c = 0;
	
	CGFloat xoffset = 0.0f;
	CGFloat xincrement = 25.0f;
	
	CGFloat xlocation = _xloc;
	CGFloat ylocation = _yloc;
	
	for (c=0;c<[_curclockstring length];++c)
	{
		singlechar = [_curclockstring characterAtIndex:c];
		
		
		if (singlechar == '0') {
			frame = [_clocknumbers objectAtIndex:0];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '1') {
			frame = [_clocknumbers objectAtIndex:1];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '2') {
			frame = [_clocknumbers objectAtIndex:2];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '3') {
			frame = [_clocknumbers objectAtIndex:3];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '4') {
			frame = [_clocknumbers objectAtIndex:4];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '5') {
			frame = [_clocknumbers objectAtIndex:5];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '6') {
			frame = [_clocknumbers objectAtIndex:6];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '7') {
			frame = [_clocknumbers objectAtIndex:7];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '8') {
			frame = [_clocknumbers objectAtIndex:8];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '9') {
			frame = [_clocknumbers objectAtIndex:9];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '.') {
			frame = [_clocknumbers objectAtIndex:10];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
	}
}

- (void) Update
{
	NSNumber *mins;
	NSNumber *hours;
	
	_dmin++;	
	_dhour++;
	
	if (_dmin > 98) {
		_dmin = 0;
		//_dhour++;
	}
	
	if (_dhour > 98) {
		_dhour = 0;
		//_dmin = 0;
	}
	
	mins = [NSNumber numberWithInt:_dmin];
	hours = [NSNumber numberWithInt:_dhour];
	
	NSMutableString *strtime;
	
	strtime = [NSMutableString new];
	
	[strtime appendString:[mins stringValue]];
	[strtime appendString:@":"];
	[strtime appendString:[hours stringValue]];
	
	_curclockstring = strtime;
	
	//[mins release];
	//[hours release];
	//[strtime release];
	
}



- (void) Paint
{
	
}


@end
